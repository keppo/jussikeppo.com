'use strict';

var gulp = require('gulp'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    maps = require('gulp-sourcemaps');

var paths = {
    html: {
      src: './',
      dest: './build/'
    },
    scss: {
        src: './scss/'
    },
    css: {
        dest: './build/'
    },
    img: {
      src: './img',
      dest: './build/'
    }
};

var out = {
    css: {
        norm: 'styles.css',
        min: 'styles.min.css'
    }
};


// ~~~~~
// Sass
// ~~~~~

gulp.task("compileSass", function() {
    return gulp.src(paths.scss.src + '*.scss')
        .pipe(maps.init())
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(maps.write('./'))
        .pipe(rename(out.css.min))
        .pipe(gulp.dest(paths.css.dest))
});

gulp.task("copyHTML", function() {
  return gulp.src(paths.html.src + '*.html')
        .pipe(gulp.dest(paths.html.dest))
});


// ~~~~~
// Defaults
// ~~~~~

gulp.task("watch", function() {
    gulp.watch(paths.scss.src + '*.scss', ['compileSass']);
});

gulp.task("build", ["compileSass", "copyHTML"], function() {});
gulp.task("default", function() {
    gulp.start("build");
});
