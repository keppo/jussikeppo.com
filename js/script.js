var $ = require('./vendor/jquery.min');

// Replaces previous use of Modernizr's JS checking
$(document).ready(function() {
    $("body").removeClass('no-js').addClass('js');
});

// Toggle mobile nav
$(document).on("click", "#mobile-nav-btn", function(event)
{
	event.preventDefault();
	$("#mobile-wrapper").toggleClass("visible");
});
$(document).on("click", "#mobile-navbar a", function(event)
{
	event.preventDefault();
	$("#mobile-wrapper").removeClass("visible");
});

// Scroll to page locations
$('#navbar a, #mobile-navbar a').on('click', function(event){
	event.preventDefault();
	var target = $(this.hash);
	$('html, body').stop().animate({
		'scrollTop': target.offset().top
	}, 1000);
});
