var webpack = require('webpack');

module.exports = {
    entry: __dirname + "/js/script.js",
    output: {
        path: __dirname + "/build",
        filename: "script.min.js"
    },
    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin(),
        new webpack.NoErrorsPlugin()
    ]
};
